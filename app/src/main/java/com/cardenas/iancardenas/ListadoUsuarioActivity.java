package com.cardenas.iancardenas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListadoUsuarioActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnVolver;
    private ListView lvUsuarios;
    private ArrayList<Usuario> lista = BaseDeDatos.obtieneListadoUsuarios();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuario);
        btnVolver = (Button)findViewById(R.id.btnVolver);
        lvUsuarios = (ListView)findViewById(R.id.lvUsuarios);
        ListAdapter adaptador = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, getDataSource());
        lvUsuarios.setAdapter(adaptador);
        btnVolver.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.finish();
    }

    public ArrayList getDataSource() {
        ArrayList<String> listaNombres = new ArrayList<>();
        for(int i=0;i<lista.size();i++)
        {
            String nombre = lista.get(i).getNombre();
            listaNombres.add(nombre);
        }
        return listaNombres;
    }
}
