package com.cardenas.iancardenas;

/**
 * Created by DUOC on 20-04-2017.
 */

public class Usuario {
    private String nombre;
    private String clave;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
