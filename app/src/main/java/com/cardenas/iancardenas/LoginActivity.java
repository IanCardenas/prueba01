package com.cardenas.iancardenas;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnIngresar,btnRegistrar;
    private EditText etUsuario,etContraseña;
    private ArrayList<Usuario> usuarios= BaseDeDatos.obtieneListadoUsuarios();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnIngresar = (Button)findViewById(R.id.btnIngresar);
        btnRegistrar = (Button)findViewById(R.id.btnRegistrar);
        etUsuario = (EditText)findViewById(R.id.etUsuario);
        etContraseña = (EditText)findViewById(R.id.etContraseña);
        btnIngresar.setOnClickListener(this);
        btnRegistrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnIngresar:
                int count=0;
                String nombre=etUsuario.getText().toString();
                String contraseña= etContraseña.getText().toString();
                if (nombre.equals("")||contraseña.equals(""))
                {
                    Toast.makeText(this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    for (int i=0;i<usuarios.size();i++) {
                        if ((usuarios.get(i).getNombre().equals(nombre) && usuarios.get(i).getClave().equals(contraseña)))
                        {
                            count=1;
                            limpiarCampos();
                            Intent intent = new Intent(this, ListadoUsuarioActivity.class);
                            startActivity(intent);
                        }
                    }
                    if(count==0){
                    Toast.makeText(this, "Credenciales incorrectas", Toast.LENGTH_SHORT).show();}
                }
                break;
            case R.id.btnRegistrar:
                limpiarCampos();
                Intent intent = new Intent(this,RegistroActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void limpiarCampos() {
        etContraseña.setText("");
        etUsuario.setText("");
        etUsuario.setFocusable(true);
    }
}
