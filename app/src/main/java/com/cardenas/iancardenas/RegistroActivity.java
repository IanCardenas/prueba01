package com.cardenas.iancardenas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etUsuario,etClave,etRepetirClave;
    private Button btnCrear,btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario = (EditText)findViewById(R.id.etUsuario);
        etClave = (EditText)findViewById(R.id.etClave);
        etRepetirClave = (EditText)findViewById(R.id.etRepetirClave);

        btnCrear = (Button)findViewById(R.id.btnCrear);
        btnVolver = (Button)findViewById(R.id.btnVolver);
        btnCrear.setOnClickListener(this);
        btnVolver.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCrear:
                String clave = etClave.getText().toString();
                String repetirClave = etRepetirClave.getText().toString();
                String usuario = etUsuario.getText().toString();
                if(clave.equals("")&&repetirClave.equals("")&&usuario.equals(""))
                {
                    Toast.makeText(this, "Ingrese todos los campos", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(clave.equals(repetirClave))
                    {
                        Usuario us= new Usuario();
                        us.setNombre(usuario);
                        us.setClave(clave);
                        BaseDeDatos.agregarUsuario(us);
                        Toast.makeText(this, "Usuario creado perfectamente: "+BaseDeDatos.obtieneListadoUsuarios().size(), Toast.LENGTH_LONG).show();
                        limpiarCampos();
                    }
                    else
                    {
                        Toast.makeText(this, "Las Claves no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.btnVolver:
                this.finish();
                break;
        }

    }

    private void limpiarCampos() {
        etRepetirClave.setText("");
        etUsuario.setText("");
        etClave.setText("");
    }
}
